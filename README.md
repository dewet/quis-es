> _Quis Es?_ -- "Who are you?" in Latin

A tiny python service to manage and serve user avatars in Gravatar-compatible format. SAML-based SSO
is used to enable self-service for users.

[![build status](https://gitlab.com/dewet/quis-es/badges/master/build.svg)](https://gitlab.com/dewet/quis-es/commits/master)
[![coverage report](https://gitlab.com/dewet/quis-es/badges/master/coverage.svg)](https://gitlab.com/dewet/quis-es/commits/master)

## Motivation

A common problem encountered in many organisations is how to consistently manage user avatars
across all internal tools they use. Gravatar is a great idea, but it adds Yet Another Thing™ for new
starters (or even old-timers!) to do/maintain, and remember yet another set of login credentials.

This project isn't intended to be a public free-for-all avatar service, but is instead to be deployed
internally to an organisation that a) use different internal tools that supports configurable Gravatar
URLs, and b) use SAML-based SSO to provision users onto web apps.

## Installation

```
$ docker build -t quis-es .
Sending build context to Docker daemon   236 kB
Step 1/x : FROM python:3.6
 ---> 787e9f4da78e
...
Step x/x : CMD gunicorn -b localhost:5000 app:app
 ---> Running in 3653f36ce7d5
 ---> 851a78054c6a
Removing intermediate container 3653f36ce7d5
Successfully built 851a78054c6a
$ docker run --rm \
  -e SAML_METADATA_URL=https://dev-xxx.oktapreview.com/app/xxx/sso/saml/metadata \
  -v /local_storage_directory/avatars/:/app/avatars/ \
  -p 5000:5000 \
  quis-es
```

The `SAML_METADATA_URL` variable needs to be pointed at your SAML IdP's metadata configuration for
this app-specific integration. The sample URL above is what Okta uses for developer apps, and hasn't
been tested with other providers. The metadata will typically look something like this:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<md:EntityDescriptor xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata" entityID="http://www.okta.com/xxx">
  <md:IDPSSODescriptor WantAuthnRequestsSigned="false" protocolSupportEnumeration="urn:oasis:names:tc:SAML:2.0:protocol">
    <md:KeyDescriptor use="signing">
      <ds:KeyInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
        <ds:X509Data>
          <ds:X509Certificate>MIIDpDCCAoygAw...certificate data...f8/NGX5eEUZ1wRNyMS</ds:X509Certificate>
        </ds:X509Data>
      </ds:KeyInfo>
    </md:KeyDescriptor>
    <md:NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress</md:NameIDFormat>
    <md:NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified</md:NameIDFormat>
    <md:SingleSignOnService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" Location="https://dev-xxx.oktapreview.com/app/xxx/xxx/sso/saml"/>
    <md:SingleSignOnService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect" Location="https://dev-xxx.oktapreview.com/app/xxx/xxx/sso/saml"/>
  </md:IDPSSODescriptor>
</md:EntityDescriptor>
```

The app expects the SAML IdP to send basic user data in the following attributes (Okta configuration example;
first column is what the data that gets sent is named and last column is the Okta attribute name):

<img src="okta_config.png" width="600px">

## API Reference

Apps can request avatars in Gravatar-compatible syntax at `/avatar/<hash>?options`, where:
- `hash` is a 32-bit md5 hex digest of the email address
- `options` include parameters like:
  - `s`: one-dimensional size of the returned avatar in pixels. `s=200` will return a 200x200 px square avatar.

## Tests

```
n/a
```

## Contributors

Merge requests via GitLab gladly welcomed.

## License

This project is released under the Apache License v2.0