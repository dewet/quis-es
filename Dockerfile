FROM python:3.6

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y xmlsec1 && \
    apt-get autoremove -y

WORKDIR /app
VOLUME /app/avatars
ADD app.py requirements.txt /app/
ADD templates /app/templates/

RUN ["pip", "install", "-r", "requirements.txt"]

EXPOSE 5000
CMD ["gunicorn", "-b", "0.0.0.0:5000", "app:app"]
