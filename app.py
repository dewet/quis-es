#!/usr/bin/env python3

import hashlib
import logging
import os
import re
import shutil
from functools import lru_cache
from random import randrange

import requests
from PIL import Image
from flask import Flask, abort, redirect, render_template, request, session, url_for, flash, send_from_directory
from flask_bootstrap import Bootstrap
from flask_bootstrap import WebCDN
from flask_login import LoginManager, UserMixin, current_user, login_required, login_user, logout_user
from raven.contrib.flask import Sentry
from saml2 import BINDING_HTTP_POST, BINDING_HTTP_REDIRECT, entity
from saml2.client import Saml2Client
from saml2.config import Config as Saml2Config

from werkzeug.exceptions import NotFound

# Standard selection of avatar sizes to generate
AVATAR_SIZES = (180, 120, 80)

# logging.basicConfig(level=logging.INFO)

app = Flask(__name__)
app.secret_key = 'something secret'  # replace with your own
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024  # avatar images bigger than 16MiB?

# Add Flask-Login extension
login_manager = LoginManager()
login_manager.init_app(app)
user_store = {}  # in-memory user cache

# Add Bootstrap extension
Bootstrap(app)
app.extensions['bootstrap']['cdns']['jquery'] = WebCDN('//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/')

# To enable Sentry integration, set your DSN via the SENTRY_DSN environment variable
sentry = Sentry(app, logging=True, level=logging.ERROR)

if not os.environ.get('SAML_METADATA_URL', None):
    raise RuntimeError('Metadata URL for SAML IdP must be specified in SAML_METADATA_URL environment variable.')


class User(UserMixin):
    def __init__(self, user_id, first_name, last_name, email, groups):
        self.id = user_id
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.avatar_hash = hashlib.md5(email.strip().lower().encode('utf-8')).hexdigest()
        self.groups = groups


@login_manager.user_loader
def load_user(user_id):
    return user_store.get(user_id, None)


@lru_cache()
def fetch_metadata() -> str:
    return requests.get(os.environ.get('SAML_METADATA_URL')).text


def saml_client():
    """
    Configure an appropriate SAML client.
    """

    acs_url = url_for("idp_initiated", _external=True)
    https_acs_url = url_for("idp_initiated", _external=True, _scheme='https')

    saml_config = Saml2Config()
    saml_config.load({
        'metadata': {
            'inline': [fetch_metadata()],
        },
        'service': {
            'sp': {
                'endpoints': {
                    'assertion_consumer_service': [
                        (acs_url, BINDING_HTTP_REDIRECT),
                        (acs_url, BINDING_HTTP_POST),
                        (https_acs_url, BINDING_HTTP_REDIRECT),
                        (https_acs_url, BINDING_HTTP_POST)
                    ],
                },
                # Don't verify that the incoming requests originate from us via
                # the built-in cache for authn request ids in pysaml2
                'allow_unsolicited': True,
                # Don't sign authn requests, since signed requests only make
                # sense in a situation where you control both the SP and IdP
                'authn_requests_signed': False,
                'logout_requests_signed': True,
                'want_assertions_signed': True,
                'want_response_signed': False,
            },
        },
    })
    saml_config.allow_unknown_attributes = True
    return Saml2Client(config=saml_config)


@app.route("/saml/sso", methods=['POST'])
def idp_initiated():
    """
    Handle SSO sign-in from the IdP side.
    """
    authn_response = saml_client().parse_authn_request_response(request.form['SAMLResponse'], entity.BINDING_HTTP_POST)
    ava = authn_response.get_identity()
    username = authn_response.get_subject().text

    try:
        user = User(username,
                    ' '.join(ava.get('first_name', [])),
                    ' '.join(ava.get('last_name', [])),
                    ' '.join(ava['email']),
                    ava.get('groups', []))
    except KeyError:
        raise RuntimeError('SAML IdP did not send required `email` field in the identity from authentication response.')
    user_store[username] = user
    session['saml_attributes'] = authn_response.ava
    login_user(user)
    # NOTE: On a production system, the RelayState MUST be validated before blindly redirecting!
    # if 'RelayState' in request.form:
    #     return redirect(request.form['RelayState'])
    if 'pre_login_destination' in session:
        return redirect(session['pre_login_destination'])
    return redirect(url_for('user'))


@app.route("/saml/login")
def sp_initiated():
    """
    Initiate SSO sign-in from our (the SP) side.
    """

    _, info = saml_client().prepare_for_authenticate()

    redirect_url = None
    # Select the IdP URL to send the AuthN request to
    for key, value in info['headers']:
        if key is 'Location':
            redirect_url = value
            break
    response = redirect(redirect_url, code=302)
    # Be entirely safe and try to prevent caching of the short-lived login redirect
    response.headers['Cache-Control'] = 'no-cache, no-store'
    response.headers['Pragma'] = 'no-cache'
    return response


@app.route("/")
def main_page():
    if current_user.is_authenticated:
        return redirect(url_for('user'))
    else:
        return redirect(url_for('sp_initiated'))


@app.route("/user")
@login_required
def user():
    return render_template('user.html', session=session, user=current_user, random=randrange(1e12, 1e13))


@app.errorhandler(401)
def error_unauthorized(_):
    session['pre_login_destination'] = request.url
    return redirect(url_for('sp_initiated'))


@app.errorhandler(404)
def error_unauthorized(_):
    return render_template('404.html')


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("main_page"))


def expand_hash_directory(hash: str) -> str:
    return '%s/%s/%s' % (hash[:2], hash[2:4], hash)


def avatar_directory(hash: str) -> str:
    return 'avatars/%s' % expand_hash_directory(hash)


@app.route('/avatar/<hash>')
def avatar(hash: str):
    if not re.match('^[a-fA-F0-9]{32}$', hash):
        flash('Invalid hash', 'danger')
        abort(404)

    try:
        return send_from_directory(directory=avatar_directory(hash), filename='180.png', mimetype='image/png')
    except NotFound:
        return send_from_directory(directory='avatars', filename='default.png', mimetype='image/png')


@app.route('/upload_image', methods=['POST'])
@login_required
def upload_image():
    # check if the post request has the file part
    if 'file' not in request.files:
        flash('No file part', 'danger')
        return redirect(request.url)
    uploaded_file = request.files['file']  # type: werkzeug.datastructures.FileStorage

    if not uploaded_file.filename:
        flash('No selected file', 'danger')
        return redirect(request.url)

    if uploaded_file.mimetype not in ['image/jpeg', 'image/png', 'image/gif']:
        flash('Not an image file type: %s' % uploaded_file.mimetype, 'danger')
        return redirect(request.url)

    if uploaded_file:
        user_directory = avatar_directory(current_user.avatar_hash)
        shutil.rmtree(user_directory, ignore_errors=True)
        os.makedirs(user_directory)
        uploaded_file.save(os.path.join(user_directory, 'original.%s' % uploaded_file.mimetype.split('/')[1]))

        create_resized_avatars(uploaded_file, user_directory)
        flash('Image uploaded successfully.', 'info')
        return redirect(url_for('user'))

    flash('Unknown error occurred', 'danger')
    return redirect(request.url)


def create_resized_avatars(original_file, output_directory):
    im = Image.open(original_file)
    shorter_size = min(im.size)
    horizontal_padding = (shorter_size - im.size[0]) // 2
    vertical_padding = (shorter_size - im.size[1]) // 2
    crop_coords = (
        -horizontal_padding,
        -vertical_padding,
        im.size[0] + horizontal_padding,
        im.size[1] + vertical_padding)
    logging.info('Cropping original size %s at %s', im.size, crop_coords)
    im = im.crop(crop_coords)
    for size in AVATAR_SIZES:
        im.resize((size, size), Image.BICUBIC).save(os.path.join(output_directory, '%03u.png' % size), 'png')


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    if port == 5000:
        app.debug = True
    app.run(host='0.0.0.0', port=port)
